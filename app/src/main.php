<?php
	// Define constants
	define('APP_DIR', __DIR__.'/../');
	
	$configFiles = [
		'/etc/gaf-feedback.ini',
		__DIR__.'/../../gaf-feedback.ini',
	];
	
	// Load basic dependencies
	require_once APP_DIR.'/vendor/autoload.php';
	require_once APP_DIR.'/src/utils.php';
	
	
	// #### Build container template
	$ct = [];
	
	// ## Configuration
	$ct['settings'] = ['app' => [
		'core' => [
			'debugging' => false,
		],
		'pathes' => [
			'var'  => __DIR__.'/../../cache',
			'http' => __DIR__.'/../../public', // Required
		],
		'db' => [
			'uri'  => null, // Required
			'user' => null, // Required
			'pass' => null, // Required
		],
		'smtp' => null,
	]];
	
	foreach($configFiles as $configFile) {
		if(is_file($configFile)) {
			$settings = parse_ini_file($configFile, true, INI_SCANNER_TYPED);
			$ct['settings']['app'] = array_replace_recursive($ct['settings']['app'], $settings);
		}
	}
	
	if($ct['settings']['app']['core']['debugging']) {
		$ct['settings']['displayErrorDetails'] = true;
		error_reporting(-1);
		ini_set('display_startup_errors', 1);
		ini_set('display_errors', 'On');
	}
	
	// ## View
	$ct['view'] = function($c) {
		return new \Feedback\View($c);
	};
	
	// ## Database
// 	$ct['db'] = function($c) {
// 		return new \PDO(
// 			$c['settings']['app']['db']['uri'],
// 			$c['settings']['app']['db']['user'],
// 			$c['settings']['app']['db']['pass'],
// 			[
// 				\PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION,
// 				\PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
// 			]
// 		);
// 	};
	
	// ## 404 Handler
	$ct['notFoundHandler'] = function($c) {
		return function($request, $response) use($c) {
			return $c->view->renderHtml("404.xhtml")->withStatus(404);
		};
	};
	
	
	// #### Build app
	$app = new \Slim\App($ct);
	$app->get ('/',           '\Feedback\HomeController:index_get'  );
	$app->get ('/vl/{course}', '\Feedback\CourseController:index_get' );
	$app->post('/vl/{course}', '\Feedback\CourseController:index_post' );
	$app->get ('/admin/vl/{course}', '\Feedback\CourseController:index_get' );
	$app->any('/superadmin/', function($req, $res, $args) {
		return $res->withRedirect($req->getUri()->getBasePath().'/superadmin/vl/', 307);
	});
	$app->get ('/superadmin/vl/', '\Feedback\CoursesController:index_get' );
	$app->get ('/superadmin/vl/add', '\Feedback\AddCourseController:index_get' );
	$app->get ('/superadmin/vl/{course_id}', '\Feedback\CourseController:index_get' );
// 	$app->post('/mails/', '\Ophase\MailsController:index_post');
// 	$app->get ('/loop/',  '\Ophase\LoopController:index_get'  );
// 	
// 	foreach(['/files', '/mails', '/loop'] as $uri) {
// 		$app->any($uri, function($req, $res, $args) use($uri) {
// 			return $res->withRedirect($req->getUri()->getBasePath().$uri.'/', 307);
// 		});
// 	}
	
	$app->run();
	