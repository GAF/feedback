<?php
	namespace Feedback;
	
	class CoursesController {
		protected $c;
		
		
		public function __construct(\Slim\Container $container) {
			$this->c = $container;
		}
		
		
		public function index_get($request, $response, $args) {
			$data = [
				'terms' => [
					['name' => 'Sommersemester 2017', 'courses' => [
						['id' => 171, 'shortName' => 'dbs', 'name' => 'Datenbanksysteme', 'deletionDate' => 1234, 'messageCount' => 42],
						['id' => 172, 'shortName' => 'rnvs', 'name' => 'Rechnernetze und Verteilte Systeme', 'deletionDate' => 1234, 'messageCount' => 11],
					]],
					['name' => 'Wintersemester 2017/18', 'courses' => [
						['id' => 181, 'shortName' => 'dbs', 'name' => 'Datenbanksysteme', 'deletionDate' => null, 'messageCount' => 44],
						['id' => 182, 'shortName' => 'rnvs', 'name' => 'Rechnernetze und Verteilte Systeme', 'deletionDate' => null, 'messageCount' => 23],
					]],
				],
			];
			return $this->c->view->renderHtml('sa_courses.xhtml', $data);
		}
	}
	