<?php
	namespace Feedback;
	
	class CourseController {
		protected $c;
		
		
		public function __construct(\Slim\Container $container) {
			$this->c = $container;
		}
		
		
		public function index_get($request, $response, $args) {
			$data = [
				'course' => ['shortName' => 'dbs', 'name' => 'Datenbanksysteme'],
				'mwnCnt' => 1,
				'pwCnt' => 1,
				'message' => '',
				'mail' => '',
				'ds' => false,
			];
			return $this->c->view->renderHtml('course.xhtml', $data);
		}
		
		public function index_post($request, $response, $args) {
			$data = [
				'course' => ['shortName' => 'dbs', 'name' => 'Datenbanksysteme'],
				'mwnCnt' => 1,
				'pwCnt' => 1,
				'message' => $_POST
			];
			$data['message'] = isset($_POST['message']) ? $_POST['message'] : '';
			$data['mail'] = isset($_POST['mail']) ? $_POST['mail'] : '';
			$data['ds'] = isset($_POST['ds']);
			return $this->c->view->renderHtml('course.xhtml', $data);
		}
	}
	