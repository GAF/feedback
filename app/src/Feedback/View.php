<?php
	namespace Feedback;
	
	class View {
		public function __construct($container) {
			$this->c = $container;
		}
		
		public function renderHtml($tpl, $data=[]) {
			$phptal = (new \PHPTAL($tpl))->setTemplateRepository([APP_DIR.'/tpl'])
				->setPhpCodeDestination($this->c->settings['app']['pathes']['var'].'/tpl/')
				->setOutputMode(\PHPTAL::HTML5)
				->stripComments(true)
				->addPreFilter(new \PHPTAL_PreFilter_Compress());
			
			// Set data
			foreach($data as $key => $value) {
				$phptal->set($key, $value);
			}
			$phptal->uri = $this->c->request->getUri();
			
			// Render
			$response = $this->c->response;
			$response->getBody()->write($phptal->execute());
			
			return $response;
		}
		
		
		public function renderPlain($text) {
			$response = $this->c->response;
			$response->getBody()->write($text);
			return $response
				->withHeader('Content-Type', 'text/plain');
		}
	}