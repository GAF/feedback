<?php
	namespace Feedback;
	
	class HomeController {
		protected $c;
		
		
		public function __construct(\Slim\Container $container) {
			$this->c = $container;
		}
		
		
		public function index_get($request, $response, $args) {
			$data = [
				'courses' => [
					['shortName' => 'dbs', 'name' => 'Datenbanksysteme'],
				],
			];
			return $this->c->view->renderHtml('index.xhtml', $data);
		}
	}
	