<?php
	function startsWith($str, $begin) {
		return substr($str, 0, strlen($begin)) === $begin;
	}
	
	
	function endsWith($str, $end) {
		return substr($str, -strlen($end)) === $end;
	}
	